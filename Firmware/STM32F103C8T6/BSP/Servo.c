#include "stm32f10x.h"                  // Device header
#include "PWM.h"
#include "Delay.h"

void Servo_Init(void)
{
	PWM_Init();
}

void Servo_SetAngle1(float Angle)
{
	PWM_SetCompare1(Angle / 180 * 2000 + 500);
}

void Servo_SetAngle2(float Angle)
{
	PWM_SetCompare2(Angle / 180 * 2000 + 500);
}

void Servo_SetAngle3(float Angle)
{
	PWM_SetCompare3(Angle / 180 * 2000 + 500);
}

void Servo_SetAngle4(float Angle)
{
	PWM_SetCompare4(Angle / 180 * 2000 + 500);
}

void Servo_SetAngle(float Angle1, float Angle2, float Angle3, float Angle4)
{
	PWM_SetCompare1(Angle1 / 180 * 2000 + 500);
	Delay_s(1);
	PWM_SetCompare2(Angle2 / 180 * 2000 + 500);
	Delay_s(1);
	PWM_SetCompare3(Angle3 / 180 * 2000 + 500);
	Delay_s(1);
	PWM_SetCompare4(Angle4 / 180 * 2000 + 500);

}

