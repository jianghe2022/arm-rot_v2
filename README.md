# README

```txt
日期：2023/05/05
撰写：jianghe
备注：机械臂教程的驱动模板
```

## 名称 Name

* 机械臂教程的驱动模板
* Drive Template for Robot Arm Tutorial

## 描述 Description

```markdown
### 机械臂教程的驱动模板
* 驱动4个舵机；
* 蓝牙串口收发。
```

## 安装 Installation

- 编译软件：Keil
- 编程语言：C
- 烧录工具：Keil STLink
- 仿真软件：无
- 主要模块
    - 舵机驱动模块：TIM4及其4个通道
    - 串口驱动模块：USART2
    - 最小系统板
        - 主控芯片：STM32F103C8T6
        - 电源模块：3.3V；5V需要外接
        - 时钟电路：72MHz
        - 复位电路
        - 烧录方式：SWD

## 用法 Usage

* 视频



## 支持 Support

* 有问题建议直接在演示或教学视频下方评论区留言

## 开发 Development

* 如果你要基于此重新开发，有以下内容可供参考
* 文档 Document：芯片资料、器件清单、B站江协科技的底层驱动等
* 固件 Firmware：修改你所需的定时器或者串口，或者增加其他内容
* 硬件 Harware： json文件可导入立创EDA软件，自定义引脚；Gerber文件可直接在嘉立创免费打板；
* 软件 Software：蓝牙APP、野火PC端多功能助手工具

## 许可 License

* 项目开源
* 禁止售卖
* 仅供学习
* 引用请注明出处